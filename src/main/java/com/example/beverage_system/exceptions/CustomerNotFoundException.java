package com.example.beverage_system.exceptions;

public class CustomerNotFoundException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3127050825962270105L;

	public CustomerNotFoundException(Long id) {
		    super("Could not find customer " + id);
		  }
		
}
