package com.example.beverage_system.exceptions;

public class ProductNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4348160831636857331L;

	public ProductNotFoundException(Long id) {
		super("Could not find product " + id);
	}

}