package com.example.beverage_system.exceptions;

public class BalanceTooLowException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5931347100198059772L;

	public BalanceTooLowException(Long id) {
		super("Balance of customer with id=" + id + " too low");
	}

}
