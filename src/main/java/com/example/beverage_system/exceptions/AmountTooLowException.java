package com.example.beverage_system.exceptions;

public class AmountTooLowException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8352473254550140933L;

	public AmountTooLowException(Long id) {
		super("Amount of Product with id " + id + "is too low");
	}

	
}
