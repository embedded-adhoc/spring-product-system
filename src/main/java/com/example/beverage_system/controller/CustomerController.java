package com.example.beverage_system.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.beverage_system.exceptions.BalanceTooLowException;
import com.example.beverage_system.exceptions.CustomerNotFoundException;
import com.example.beverage_system.exceptions.ProductNotFoundException;
import com.example.beverage_system.model.Customer;
import com.example.beverage_system.model.ERole;
import com.example.beverage_system.model.Product;
import com.example.beverage_system.model.Role;
import com.example.beverage_system.repositories.CustomerRepository;
import com.example.beverage_system.repositories.ProductRepository;
import com.example.beverage_system.repositories.RoleRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class CustomerController {

	private final CustomerRepository repository;

	private final ProductRepository productRepository;

	private final RoleRepository roleRepository;

	CustomerController(CustomerRepository repository, ProductRepository productRepository,
			RoleRepository roleRepository) {
		this.repository = repository;
		this.productRepository = productRepository;
		this.roleRepository = roleRepository;
	}

	@GetMapping("/customers")
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	List<Customer> getAllCustomers() {
		System.out.println("Requested all Customers. Found the following:");
		for (Customer c : repository.findAll()) {
			c.getRolesInString();
			System.out.println(c.toString());
		}
		System.out.println('\n');
		System.out.println(repository.findAll());
		return repository.findAll();
	}

	/*
	 * Request Body Typ: raw .. json
	 * 
	 * body: { "id": 8, "firstName": "Na", "lastName": "Moin", "balance": 100.0 }
	 */
	@PostMapping("/customers")
	@PreAuthorize("hasRole('ADMIN')")
	Customer newCustomer(@RequestBody Customer newCustomer) {
		System.out.println("Inserted new Customer:" + newCustomer.toString());
		System.out.println('\n');
		return repository.save(newCustomer);
	}

	@GetMapping("/customers={id}")
	@PreAuthorize("hasRole ('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	Customer getOneCustomer(@PathVariable Long id) {
		System.out.println("Customer with id=" + id + ":" + repository.findById(id).toString());
		System.out.println('\n');
		return repository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
	}

	/*
	 * Request Body Typ: raw .. json
	 * 
	 * body: { "firstName": "Na", "lastName": "Moin", "balance": 100.0 }
	 */
	@PutMapping("/customers={id}")
	// @PreAuthorize("hasRole('MOD') or hasRole('ADMIN')")
	Customer replaceCustomer(@RequestBody Customer newCustomer, @PathVariable Long id) {
		return repository.findById(id).map(customer -> {
			customer.setFirstName(newCustomer.getFirstName());
			customer.setLastName(newCustomer.getLastName());
			customer.setBalance(newCustomer.getBalance());
			customer.setUsername(newCustomer.getUsername());
			customer.setEmail(newCustomer.getEmail());

			System.out.println("Controller: Customer with id " + id + " updated/replaced! ");
			System.out.println("New attributes: " + customer.toString());
			System.out.println('\n');

			return repository.save(customer);

		}).orElseGet(() -> {
			newCustomer.setId(id);
			System.out.println("Controller: Customer with id " + id
					+ " not found. No update possible. inserted new Customer with id " + id);
			System.out.println("Attributes: " + newCustomer.toString());
			System.out.println('\n');

			return repository.save(newCustomer);
		});
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/customers/roles={id}")
	void updateUserRights(@RequestBody String newRole, @PathVariable Long id) {

		// Set<String> strRoles = updateUserRightsRequest.getNewRoles();

		System.out.println("REPLACE ROLES:" + newRole);
		Set<Role> roles = new HashSet<>();

		if (newRole == null || newRole.isEmpty()) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			switch (newRole) {
			case "admin":
				Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(adminRole);
				break;

			case "mod":
				Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(modRole);
				break;

			case "mod, admin":
				Role modAdminRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(modAdminRole);
				Role adminModRole = roleRepository.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(adminModRole);
				break;

			case "admin, mod":
				Role maRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(maRole);
				Role amRole = roleRepository.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(amRole);
				break;

			default:
				Role userRole = roleRepository.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(userRole);

			}

		}

		Optional<Customer> tmpCustomer = repository.findById(id);

		Customer currentCustomer = (Customer) tmpCustomer.get();

		currentCustomer.setRoles(roles);

		repository.save(currentCustomer);

	}

	@DeleteMapping("/customers={id}")
	@PreAuthorize("hasRole('ADMIN')")
	void deleteOneCustomer(@PathVariable Long id) {
		repository.deleteById(id);
		System.out.println("Controller: Customer with id " + id + " deleted!");
		System.out.println('\n');
	}

	// subtract products's price from customer's balance
	@PutMapping("/customers={customerId}/products={productId}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	Customer customerBoughtProduct(@PathVariable Long productId, @PathVariable Long customerId) throws Exception {
		return repository.findById(customerId).map(customer -> {
			Product boughtProduct = productRepository.findById(productId)
					.orElseThrow(() -> new ProductNotFoundException(productId));
			// minimum balance = -10. no further transactions possible if minimum reached
			if ((customer.getBalance() - boughtProduct.getPrice()) >= -10) {
				customer.setBalance(customer.getBalance() - boughtProduct.getPrice());
				// decrease amount of bought product
				boughtProduct.setAmount(boughtProduct.getAmount() - 1);
				System.out.println("controller: new customer balance: " + customer.getBalance());
				System.out.println("controller: new product amount: " + boughtProduct.getAmount());
				System.out.println('\n');
				return repository.save(customer);
			} else {

				throw new BalanceTooLowException(customerId);
				// return repository.findById(customerId).orElseThrow(() -> new
				// CustomerNotFoundException(customerId));

			}
		}).orElseThrow(() -> new CustomerNotFoundException(customerId));

	}

	@DeleteMapping("/customers")
	@PreAuthorize("hasRole('ADMIN')")
	void deleteAllCustomers() {
		System.out.println("Deleting ALL customers..." + '\n');
		repository.deleteAll();
	}

}
