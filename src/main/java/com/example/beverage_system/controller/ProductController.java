package com.example.beverage_system.controller;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.beverage_system.exceptions.ProductNotFoundException;
import com.example.beverage_system.model.Product;
import com.example.beverage_system.repositories.ProductRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ProductController {

	private final ProductRepository repository;

	ProductController(ProductRepository repository) {
		this.repository = repository;
	}

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	List<Product> getAllProducts() {
		return repository.findAll();
	}

	@RequestMapping(value = "/products", method = RequestMethod.POST)
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	Product newProduct(@RequestBody Product newProduct) {
		return repository.save(newProduct);
	}

	@RequestMapping(value = "/products={id}", method = RequestMethod.GET)
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	Product getOneProduct(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
	}

	@RequestMapping(value = "/products={id}", method = RequestMethod.PUT)
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	Product replaceProduct(@RequestBody Product newProduct, @PathVariable Long id) {
		return repository.findById(id).map(product -> {
			product.setName(newProduct.getName());
			product.setPrice(newProduct.getPrice());
			product.setAmount(newProduct.getAmount());
			return repository.save(product);
		}).orElseGet(() -> {
			newProduct.setId(id);
			return repository.save(newProduct);
		});
	}

	@RequestMapping(value = "/products={id}", method = RequestMethod.DELETE)
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	void deleteProduct(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
	@DeleteMapping("/products")
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	void deleteAllProducts() {
		System.out.println("Deleting ALL products..." + '\n');
		repository.deleteAll();
	}

}
