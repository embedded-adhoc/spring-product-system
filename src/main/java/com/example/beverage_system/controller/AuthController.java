package com.example.beverage_system.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.beverage_system.model.Customer;
import com.example.beverage_system.model.ERole;
import com.example.beverage_system.model.Role;
import com.example.beverage_system.payload.request.ChangePasswordRequest;
import com.example.beverage_system.payload.request.LoginRequest;
import com.example.beverage_system.payload.request.SignupRequest;
import com.example.beverage_system.payload.response.JwtResponse;
import com.example.beverage_system.payload.response.MessageResponse;
import com.example.beverage_system.repositories.CustomerRepository;
import com.example.beverage_system.repositories.RoleRepository;
import com.example.beverage_system.security.jwt.JwtUtils;
import com.example.beverage_system.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	CustomerRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		System.out.println("went to /signin");

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(
				new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		System.out.println("went to /signup");
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		System.out.println("Found no existing customer, create new customer");
		Customer user = new Customer(0L, signUpRequest.getUsername(), signUpRequest.getUserLastName(), (float) 0,
				signUpRequest.getUsername(), signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		String rolePerText = signUpRequest.getRolePerText();

		System.out.println("roles per text : " + rolePerText);
		System.out.println("strRoles: " + strRoles);
		System.out.println(strRoles.size());
		System.out.println("roles (hashset): " + roles);

		if (strRoles == null || strRoles.isEmpty()) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);
					break;

				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);
					break;

				case "mod, admin":
					Role modAdminRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modAdminRole);
					Role adminModRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminModRole);
					break;

				case "admin, mod":
					Role maRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(maRole);
					Role amRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(amRole);
					break;

				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);

				}
			});

		}

		user.setRoles(roles);
		userRepository.save(user);

		System.out.println("Roles:" + roles);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
	
	@PostMapping("/pass")
	public ResponseEntity<?> changeUserPassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
		System.out.println("went to /pass");

		// get current Customer from username
		Optional<Customer> currentUser = userRepository.findByUsername(changePasswordRequest.getUsername());
		// cast this Optional to be a Customer
		Customer currentCustomer = (Customer) currentUser.get();

		// compare, if given password matches with stored password of user. if true
		// continue, if false: abort
		if (encoder.matches(changePasswordRequest.getCurrentPassword(), currentCustomer.getPassword())) {

			currentCustomer.setPassword(encoder.encode(changePasswordRequest.getNewPassword()));

			userRepository.save(currentCustomer);
			System.out.println("Password changed to: " + currentCustomer.getPassword());

			// log the user in:
			Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					changePasswordRequest.getUsername(), changePasswordRequest.getNewPassword()));

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);

			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
			List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
					.collect(Collectors.toList());

			return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
					userDetails.getEmail(), roles));

		} else {
			System.out.println("Current password was checked and is not valid.");
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Error: Current password was checked and is not valid."));

		}
	}

	@PostMapping("/reset")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> resetUserPassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
		System.out.println("went to /reset");

		// get current Customer from username
		Optional<Customer> currentUser = userRepository.findByUsername(changePasswordRequest.getUsername());
		// cast this Optional to be a Customer
		Customer currentCustomer = (Customer) currentUser.get();

		System.out.println(currentCustomer.getUsername());

		currentCustomer.setPassword(encoder.encode(changePasswordRequest.getNewPassword()));

		userRepository.save(currentCustomer);
		System.out.println("Password changed to: " + currentCustomer.getPassword());

		return ResponseEntity.ok().body(new MessageResponse("Success"));

	}
}