package com.example.beverage_system.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.beverage_system.model.HistoryEntry;
import com.example.beverage_system.repositories.HistoryEntryRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class HistoryEntryController {

	private final HistoryEntryRepository repository;

	HistoryEntryController(HistoryEntryRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/timestamps")
	@PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
	List<HistoryEntry> getAllTimeStamps() {
		System.out.println("get full history");
		for (HistoryEntry t : repository.findAll())
			System.out.println(t.toString());
		System.out.println('\n');
		return repository.findAll();
	}
	
	@GetMapping("/timestamps={id}")
	List<HistoryEntry> getUserHistory(@PathVariable Long id){
		System.out.println("Get user history");
		List <HistoryEntry> entries = new ArrayList<>();
		for(HistoryEntry h: repository.findAll()) {
			if(h.customer_id == id) {
				entries.add(h);
				System.out.println(h.toString());
			}
		}
		return entries;
	}

	@PostMapping("/timestamps")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	HistoryEntry newHistoryEntry(@RequestBody HistoryEntry newHistoryEntry) {

		System.out.println("Inserted new HistoryEntry:" + newHistoryEntry.toString());
		System.out.println('\n');
		return repository.save(new HistoryEntry(newHistoryEntry.getCustomer_id(), newHistoryEntry.getProduct_id(), newHistoryEntry.getProduct_name(),
				newHistoryEntry.getProduct_price(), newHistoryEntry.getProduct_orderAmount(), newHistoryEntry.getCustomer_balanceBefore(),
				newHistoryEntry.getCustomer_balanceAfter()));
	}

}
