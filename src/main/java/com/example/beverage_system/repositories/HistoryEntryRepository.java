package com.example.beverage_system.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.beverage_system.model.HistoryEntry;

@Repository
public interface HistoryEntryRepository extends MongoRepository<HistoryEntry, Long> {

}
