package com.example.beverage_system.model;

import java.time.Instant;
import java.util.Objects;

public class HistoryEntry {

	public Instant date;

	public Long customer_id;

	public Long product_id;
	
	public String product_name;

	public Float product_price;
	
	public int product_orderAmount;

	public Float customer_balanceBefore;

	public Float customer_balanceAfter;

	@Override
	public int hashCode() {
		return Objects.hash(date, customer_id, product_id, product_price, customer_balanceBefore,
				customer_balanceAfter);
	}

	public HistoryEntry() {

	}

	
	public HistoryEntry(Long customer_id, Long product_id, String product_name, Float product_price,
			int product_orderAmount, Float customer_balanceBefore, Float customer_balanceAfter) {
		super();
		this.date = Instant.now();
		this.customer_id = customer_id;
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_orderAmount = product_orderAmount;
		this.customer_balanceBefore = customer_balanceBefore;
		this.customer_balanceAfter = customer_balanceAfter;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public Long getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}

	public Float getProduct_price() {
		return product_price;
	}

	public void setProduct_price(Float product_price) {
		this.product_price = product_price;
	}

	public Float getCustomer_balanceBefore() {
		return customer_balanceBefore;
	}

	public void setCustomer_balanceBefore(Float customer_balanceBefore) {
		this.customer_balanceBefore = customer_balanceBefore;
	}

	public Float getCustomer_balanceAfter() {
		return customer_balanceAfter;
	}

	public void setCustomer_balanceAfter(Float customer_balanceAfter) {
		this.customer_balanceAfter = customer_balanceAfter;
	}
	

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public int getProduct_orderAmount() {
		return product_orderAmount;
	}

	public void setProduct_orderAmount(int product_orderAmount) {
		this.product_orderAmount = product_orderAmount;
	}

	@Override
	public String toString() {
		return "HistoryEntry [date=" + date + ", customer_id=" + customer_id + ", product_id=" + product_id
				+ ", product_name=" + product_name + ", product_price=" + product_price + ", product_orderAmount="
				+ product_orderAmount + ", customer_balanceBefore=" + customer_balanceBefore
				+ ", customer_balanceAfter=" + customer_balanceAfter + "]";
	}

}
