package com.example.beverage_system.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customers", uniqueConstraints = { @UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email") })
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	public Long id;

	@NotBlank
	@Column(name = "firstName")
	public String firstName;

	@NotBlank
	@Column(name = "lastName")
	public String lastName;

	@NotBlank
	@Column(name = "balance")
	public Float balance;

	@NotBlank
	@Size(max = 20)
	@Column
	private String username;

	@NotBlank
	@Size(max = 50)
	@Email
	@Column
	private String email;

	@NotBlank
	@Size(max = 120)
	@Column
	private String password;

	@ManyToMany(fetch = FetchType.LAZY)
	// @JoinTable(name = "customer_roles", joinColumns = @JoinColumn(name =
	// "customer_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))

	@JoinTable(name = "customers_roles", joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))

	private Set<Role> roles = new HashSet<>();

	public Customer() {

	}

	public Customer(Long id, @NotBlank String firstName, @NotBlank String lastName, @NotBlank Float balance,
			@NotBlank String username, @NotBlank String email, @NotBlank String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.balance = balance;
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Float getBalance() {
		return balance;
	}

	public void setBalance(Float balance) {
		this.balance = balance;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Column
	@ElementCollection(targetClass = String.class)
	private List<String> rolesInString;

	public List<String> getRolesInString() {
		Set<Role> tmp = this.getRoles();
		for (Role r : tmp) {
			this.rolesInString.add(r.getName().toString());
		}
		return this.rolesInString;
	}

	public void setRolesInString(List<String> rolesInString) {
		this.rolesInString = rolesInString;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", balance=" + balance
				+ ", username=" + username + ", email=" + email + ", password=" + password + ", roles=" + roles
				+ ", rolesInString=" + rolesInString + "]";
	}

}
