package com.example.beverage_system;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.beverage_system.model.Customer;
import com.example.beverage_system.model.Product;
import com.example.beverage_system.model.Role;
import com.example.beverage_system.model.HistoryEntry;
import com.example.beverage_system.repositories.CustomerRepository;
import com.example.beverage_system.repositories.ProductRepository;
import com.example.beverage_system.repositories.RoleRepository;
import com.example.beverage_system.repositories.HistoryEntryRepository;

@SpringBootApplication
public class BeverageSystemApplication implements CommandLineRunner {

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private ProductRepository productRepository;

	// would be nice to get rid of this, but it prevents a BeanCreationException due
	// to moving the package
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private HistoryEntryRepository timeStampRepository;

	public static void main(String[] args) {
		SpringApplication.run(BeverageSystemApplication.class, args);
	}

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void run(String... strings) throws Exception {

		List<Product> products = productRepository.findAll();
		List<Customer> customers = customerRepository.findAll();
		List<Role> roles = roleRepository.findAll();
		List<HistoryEntry> timeStamps = timeStampRepository.findAll();

		for (Product p : products)
			log.info("Received " + p.toString());

		for (Customer c : customers)
			log.info("Received " + c.toString());

		for (Role r : roles)
			log.info("Received" + r.toString());
		
		for (HistoryEntry t : timeStamps)
			log.info("Received" + t.toString());

	}

}