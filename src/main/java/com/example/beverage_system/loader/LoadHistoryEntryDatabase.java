package com.example.beverage_system.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.beverage_system.model.HistoryEntry;
import com.example.beverage_system.repositories.HistoryEntryRepository;

@Configuration
public class LoadHistoryEntryDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadHistoryEntryDatabase.class);

	@Bean
	CommandLineRunner initTimeStampDatabase(HistoryEntryRepository historyEntryRepository) {

		if (historyEntryRepository.count() > 0) {
			return args -> {
				for (HistoryEntry c : historyEntryRepository.findAll())
					log.info("Existing entries: " + c.toString());
			};
		}

		else {
			return args -> {
				/*
				 * public HistoryEntry(Long customer_id, Long product_id, Float product_price,
				 * Float customer_balanceBefore, Float customer_balanceAfter)
				 */
				// HistoryEntry histEn = new HistoryEntry(2L, 2L, (float) 1.5, (float) 1.5,
				// (float) 1.5);

				log.info("Preloading " + historyEntryRepository
						.save(new HistoryEntry(7L, 2L, "dummyProduct", (float) 1.5, 3, (float) 20, (float) 11)));
			};
		}

	}
}