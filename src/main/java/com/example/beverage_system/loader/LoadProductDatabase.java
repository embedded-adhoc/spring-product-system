package com.example.beverage_system.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.beverage_system.model.Product;
import com.example.beverage_system.repositories.ProductRepository;

@Configuration
public class LoadProductDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadProductDatabase.class);

	@Bean
	CommandLineRunner initProductDatabase(ProductRepository productRepository) {

		if (productRepository.count() > 0) {
			return args -> {
				for (Product c : productRepository.findAll())
					log.info("Existing entries: " + c.toString());
			};
		}

		else {

			return args -> {

				// Long id, String name, float price, int amount) {
				log.info("Preloading " + productRepository.save(new Product(0L, "Coca Cola klein", (float) 1.5, 4)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Coca Cola groß", (float) 2, 3)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Fanta", (float) 2, 3)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Mezzo Mix", (float) 2, 3)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Apfelschorle klein", (float) 1.5, 4)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Mineralwasser STILL", (float) 2, 9)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Mineralwasser MEDIUM klein", (float) 1.5, 3)));
				log.info("Preloading " + productRepository.save(new Product(0L, "Mineralwasser MEDIUM groß", (float) 2, 6)));

			};
		}
	}
}