package com.example.beverage_system.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.beverage_system.model.Customer;
import com.example.beverage_system.model.ERole;
import com.example.beverage_system.model.Role;
import com.example.beverage_system.repositories.CustomerRepository;

@Configuration
public class LoadCustomerDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadCustomerDatabase.class);

	@Bean
	CommandLineRunner initCustomerDatabase(CustomerRepository customerRepository) {

		// customerDB is alive
		if (customerRepository.count() > 0) {
			return args -> {
				for (Customer c : customerRepository.findAll()) {
					log.info("Existing entries: " + c.toString());
					/*
					 * for (Role role : c.getRoles()) { log.info(c.getFirstName() + "'s ROLES:" +
					 * role.getName().toString()); }
					 */
				}
			};
		}

		else {
			return args -> {
				log.info("Preloading " + customerRepository.save(
						new Customer(0L, "Ole", "Ostermann", (float) 100, "ole", "ostermann@ad-hoc.com", "123456789")));
				log.info("Preloading " + customerRepository.save(
						new Customer(0L, "Lea", "Hennemann", (float) 100, "lea", "hennemann@ad-hoc.com", "123456789")));
				log.info("Preloading " + customerRepository.save(
						new Customer(0L, "Michael", "Ebert", (float) 100, "michael", "ebert@ad-hoc.com", "123456789")));
				log.info("Preloading " + customerRepository.save(
						new Customer(0L, "Rico", "Flaegel", (float) 100, "rico", "flaegel@ad-hoc.com", "123456789")));
				log.info("Preloading " + customerRepository.save(
						new Customer(0L, "Sener", "Sahin", (float) 100, "sener", "sahin@ad-hoc.com", "123456789")));
				log.info("Preloading " + customerRepository
						.save(new Customer(0L, "Mehdi", "Kaf", (float) 100, "mehdi", "kaf@ad-hoc.com", "123456789")));
				log.info("Preloading " + customerRepository.save(new Customer(0L, "Bastian", "Rogmann", (float) 100,
						"bastian", "rogmann@ad-hoc.com", "123456789")));
			};
		}

	}
}