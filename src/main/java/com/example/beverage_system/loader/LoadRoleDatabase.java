package com.example.beverage_system.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.beverage_system.model.ERole;
import com.example.beverage_system.model.Role;
import com.example.beverage_system.repositories.RoleRepository;

@Configuration
public class LoadRoleDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadRoleDatabase.class);

	@Bean
	CommandLineRunner initRoleDatabase(RoleRepository roleRepository) {
		
		if (roleRepository.count() > 0) {
			return args -> {
				for (Role r : roleRepository.findAll())
					log.info("Existing entries: " + r.toString());
			};
		}

		else {
			return args -> {				
				log.info("Preloading " + roleRepository.save(new Role(ERole.ROLE_USER)));
				log.info("Preloading " + roleRepository.save(new Role(ERole.ROLE_MODERATOR)));
				log.info("Preloading " + roleRepository.save(new Role(ERole.ROLE_ADMIN)));
			};
		}

	}
}