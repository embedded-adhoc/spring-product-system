package com.example.beverage_system;

import com.example.beverage_system.controller.CustomerController;
import com.example.beverage_system.controller.ProductController;
import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.example.beverage_system.model.Customer;
import com.example.beverage_system.model.Product;
import com.example.beverage_system.repositories.CustomerRepository;
import com.example.beverage_system.repositories.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class BeverageSystemApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private CustomerController customerController;

	@Autowired
	private ProductController productController;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private ProductRepository productRepository;

	private Customer jules = new Customer(0L, "Jules", "Verne", (float) 25, "jules", "verne@ad-hoc.com", "123456789");
	private Customer phileas = new Customer(0L, "Phileas", "Fogg", (float) 20000, "phileas", "fogg@ad-hoc.com",
			"123456789");
	private Customer jean = new Customer(0L, "Jean", "Passepartout", (float) 81, "jean", "passepartout@ad-hoc.com",
			"123456789");

	private Product cocaCola = new Product(0L, "Coca Cola", (float) 2, 10);
	private Product sprite = new Product(0L, "Sprite", (float) 5, 78);
	private Product fanta = new Product(0L, "Fanta", (float) 1, 60);

	public static long julesId;
	public static long cocaColaId;

	private ObjectMapper mapper = new ObjectMapper();

	private String BASE_URL = "http://localhost:8181";
	private String customersUrl = BASE_URL + "/customers";
	private String productsUrl = BASE_URL + "/products";

	void saveJules() {
		jules = customerRepository.save(jules);
		julesId = jules.getId();
	}

	void saveCocaCola() {
		cocaCola = productRepository.save(cocaCola);
		cocaColaId = cocaCola.getId();
	}

	@BeforeEach
	public void initEach() {
		System.out.println("New Test started");

		// refresh database
		customerRepository.deleteAll();
		productRepository.deleteAll();

		// save two test entries in database
		saveJules();
		saveCocaCola();
	}

	// ______________________________________________Customer__________________________________________________________________________________

	@Test
	void test_customerContextLoads() {
		assertThat(customerController).isNotNull();
	}

	@Test
	void test_getAllCustomers() throws Exception {
		mockMvc.perform(get(customersUrl)).andExpect(status().isOk());
	}

	@Test
	void test_newCustomer() throws Exception {
		mockMvc.perform(
				post(customersUrl).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(phileas)))
				.andExpect(status().isOk());
	}

	@Test
	void test_getOneCustomer() throws Exception {
		mockMvc.perform(get(customersUrl + "=" + julesId)).andExpect(status().isOk());
	}

	@Test
	void test_replaceCustomerWithMatchingId() throws Exception {
		mockMvc.perform(put(customersUrl + "=" + julesId).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(jean))).andExpect(status().isOk());
	}

	@Test
	void test_replaceCustomerWithNewId() throws Exception {
		mockMvc.perform(put(customersUrl + "=" + 888).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(jean))).andExpect(status().isOk());
	}

	@Test
	void test_customerBoughtProductNoException() throws Exception {
		mockMvc.perform(put(customersUrl + "=" + julesId + "/products=" + cocaColaId)).andExpect(status().isOk());

		System.out.println("Test cola : " + productRepository.getOne(cocaColaId).getAmount());
		System.out.println("Test customer balance:" + customerRepository.getOne(julesId).getBalance());

		assertThat(productRepository.getOne(cocaColaId).getAmount() == 9);
		assertThat(customerRepository.getOne(julesId).getBalance() == 23);
	}

	@Test
	void test_deleteOneCustomer() throws Exception {
		mockMvc.perform(delete(customersUrl + "=" + julesId)).andExpect(status().isOk());
	}

	// ______________________________________________________Products___________________________________________________________________________

	@Test
	void test_productContextLoads() {
		assertThat(productController).isNotNull();
	}

	// @Test
	void test_getAllProducts() throws Exception {
		mockMvc.perform(get(productsUrl)).andExpect(status().isOk());
	}

	@Test
	void test_newProduct() throws Exception {
		mockMvc.perform(
				post(productsUrl).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(sprite)))
				.andExpect(status().isOk());
	}

	@Test
	void test_getOneProduct() throws Exception {
		mockMvc.perform(get(productsUrl + "=" + cocaColaId)).andExpect(status().isOk());
	}

	// TODO ab hier
	@Test
	void test_replaceProductWithMatchingId() throws Exception {
		mockMvc.perform(put(productsUrl + "=" + cocaColaId).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(fanta))).andExpect(status().isOk());
	}

	@Test
	void test_replaceProductWithNewId() throws Exception {
		mockMvc.perform(put(productsUrl + "=" + 888).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(fanta))).andExpect(status().isOk());
	}

	@Test
	void test_deleteOneProduct() throws Exception {
		mockMvc.perform(delete(productsUrl + "=" + cocaColaId)).andExpect(status().isOk());
	}

}
